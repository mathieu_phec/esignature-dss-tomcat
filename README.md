# Esignature DSS Tomcat Bundle

## Use

* docker run --rm -it -p 8080:8080 registry.gitlab.com/mlysakowski/esignature-dss-tomcat
* Go to http://localhost:8080/esignature-dss-bundle/

## Build

* docker build . -t registry.gitlab.com/mlysakowski/esignature-dss-tomcat
* docker push registry.gitlab.com/mlysakowski/esignature-dss-tomcat