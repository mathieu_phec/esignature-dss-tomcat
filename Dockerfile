FROM tomcat

# Get latest Esignature DSS Bundle
WORKDIR /tmp
RUN wget https://ec.europa.eu/cefdigital/artifact/repository/esignaturedss/eu/europa/ec/joinup/sd-dss/dss-demo-bundle/5.4/dss-demo-bundle-5.4.zip
RUN unzip dss-demo-bundle-5.4.zip
RUN cp dss-demo-bundle-5.4/apache-tomcat-8.5.35/webapps/ROOT.war /usr/local/tomcat/webapps/esignature-dss-bundle.war

# Add config for local Tomcat
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml

EXPOSE 8080